package mpp.SwingWorker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import mpp.imagenes.Controller;
import mpp.imagenes.db.LabelledImage;
import mpp.imagenes.labels.LabelDetector.Label;

public class DoneAutoLabelSwingWorker extends SwingWorker<Map<LabelledImage,List<Label>>, Void> {
	private Controller controller;
	private int numImg;
	
	public DoneAutoLabelSwingWorker(Controller c, int numImg) {
		this.controller = c;
		this.numImg = numImg;
	}
	
	@Override
	protected Map<LabelledImage, List<Label>> doInBackground() throws Exception {
		Map<LabelledImage,List<Label>> wait = new HashMap<>();
		for (int i = 0; i < numImg; i++) {
			if(controller.canceledPool())
				return wait;
			LabelledImage img = controller.getDB().get(i);
			System.out.println("AutoLabel: "+img.getFile().getAbsolutePath());
			List<Label> labels = controller.getLabel(img.getFile());
			if(labels == null)
				continue;
			else {
				wait.put(img, labels);
			}
		}
		return wait;
	}
	
	@Override
	protected void done() {
		try {
			if(controller.canceledPool())
				return;
			Map<LabelledImage,List<Label>> wait = get();
			for (LabelledImage img : wait.keySet()) {
				System.out.println("Procesando: "+img.getFile().getAbsolutePath());
				if(controller.isCurrentImg(img)) {
					controller.forceUpdate();
				}
				for (Label label : wait.get(img)) {
					controller.addLabel(img,label.getValue());
				}
				controller.incrementWaitTask();
				if(controller.isCurrentImg(img)) {
					controller.updateTextPanel();
				}
			}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
