package mpp.SwingWorker;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import java.util.LinkedList;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import mpp.imagenes.Controller;

public class DoneLoadImageSwingWorker  extends SwingWorker<Map<File,Image>, Void>{
	private List<File> files;
	private int width;
	private int height;
	private Controller controller;

	private List<String> error;


//	Variables necesarias
	public DoneLoadImageSwingWorker(@Nonnull List<File> files, int width, int height, Controller controller) {
		this.files = files;
		this.width = width;
		this.height = height;
		this.controller = controller;

		error = new LinkedList<>();

	}

	@Override
	protected Map<File, Image> doInBackground() throws Exception {
		Map<File,Image> map = new HashMap<>();
//		Recorremos la lista de ficheros
		for (File file : files) {
			if(controller.canceledPool())
				return map;
//			Leemos el fichero actual
			System.out.println("Leyendo: " + file);
			BufferedImage img = ImageIO.read(file);
			if (img == null) {
				error.add(file.getAbsolutePath());
				controller.incrementWaitTask();
				System.out.println("Error: "+file.getAbsolutePath());
				continue;
			}
//			Guardamos la imagen
			map.put(file, img.getScaledInstance(width, height, Image.SCALE_FAST));
		}
		return map;
	}
	
	
	@Override
	protected void done() {
		Map<File, Image> map;
		try {
			map = get();
//			Recorremos las imagenes cargadas, y las guardamos mediante el uso del hilo de eventos
			for (File file : map.keySet()) {
				System.out.println("Procesando: " + file);
				controller.saveImg(file, map.get(file));
			}
			if(error.size() > 0) {
				controller.addError(error);
				controller.updateInfo();
			}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
