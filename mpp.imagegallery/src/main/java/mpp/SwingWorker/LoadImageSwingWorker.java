package mpp.SwingWorker;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import mpp.imagenes.Controller;

public class LoadImageSwingWorker extends SwingWorker<Boolean, Void> {

	private File file;
	private int width;
	private int height;
	private Controller controller;
	
//	Variables necesarias
	public LoadImageSwingWorker(@Nonnull File files,int width,int height, Controller controller) {
		this.file = files;
		this.width = width;
		this.height = height;
		this.controller = controller;
	}
	
	@Override
	protected Boolean doInBackground() throws Exception {
//		Leemos la imagen
		System.out.println("Processing: " + file);
		BufferedImage img = ImageIO.read(file);
		if (img == null)
			return false;
//		La guardamos, podemos hacer esto debido a que el mtodo es syncrhonize
		controller.saveImg(file,img.getScaledInstance(width, height, Image.SCALE_FAST));
		return true;
	}
	
	@Override
	protected void done() {
		try {
			if(!get()) {
				List<String> error = new LinkedList<>();
				error.add(file.getAbsolutePath());
				controller.addError(error);
				controller.incrementWaitTask();
				System.out.println("Error "+file.getAbsolutePath());
			}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
