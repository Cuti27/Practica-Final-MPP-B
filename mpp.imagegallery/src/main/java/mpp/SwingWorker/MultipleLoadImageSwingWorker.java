package mpp.SwingWorker;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import mpp.imagenes.Controller;

public class MultipleLoadImageSwingWorker extends SwingWorker<Void, Void> {

	private List<File> files;
	private int width;
	private int height;
	private Controller controller;
	private List<String> error;

//	Variables necesarias
	public MultipleLoadImageSwingWorker(@Nonnull List<File> files, int width, int height, Controller controller) {
		this.files = files;
		this.width = width;
		this.height = height;
		this.controller = controller;
		error = new LinkedList<>();
	}

	@Override
	protected Void doInBackground() throws Exception {
//		Recorremos la lista de ficheros
		for (File file : files) {
			if(controller.canceledPool())
				return null;
//			Leemos el fichero actual
			System.out.println("Processing: " + file);
			BufferedImage img = ImageIO.read(file);
			if (img == null) {
				error.add(file.getAbsolutePath());
				controller.incrementWaitTask();
				System.out.println("Error: "+file.getAbsolutePath());
				continue;
			}
//			Guardamos la imagen
			controller.saveImg(file, img.getScaledInstance(width, height, Image.SCALE_FAST));
		}
		return null;
	}
	
//	Funci�n para actualizar
	@Override
	protected void process(List<Void> chunks) {
		controller.updateInfo();
	}
	
	@Override
	protected void done() {
		System.out.println("Errores totales: "+error.size());
		if(error.size() > 0) {
			controller.addError(error);
			controller.updateInfo();
		}
	}


}
