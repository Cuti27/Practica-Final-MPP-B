package mpp.SwingWorker;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import mpp.imagenes.Controller;
import mpp.imagenes.db.LabelledImage;

public class TransformSwingWorker extends SwingWorker<Void, Void>{

	private File folder;
	private LabelledImage labelledImage;
	private Controller controller;
	
	public TransformSwingWorker(File folder, LabelledImage labelledImage, Controller controller) {
		this.folder = folder;
		this.labelledImage = labelledImage;
		this.controller = controller;
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		File f = labelledImage.getFile();
		System.out.println("Transform: "+f.getAbsolutePath());
		try {
			BufferedImage image = ImageIO.read(f);
			float[] blurMatrix = { 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f,
					1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f };
			BufferedImageOp blurFilter = new ConvolveOp(new Kernel(3, 3, blurMatrix), ConvolveOp.EDGE_NO_OP, null);

			BufferedImage filtered = blurFilter.filter(image, null);

			File outputFileName = Paths.get(folder.getPath(), f.getName()).toFile();
			ImageIO.write(filtered, "jpg", outputFileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		controller.incrementWaitTask();
		return null;
	}
	

}
