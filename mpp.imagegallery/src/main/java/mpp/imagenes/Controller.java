package mpp.imagenes;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import javax.imageio.ImageIO;

import mpp.SwingWorker.AutoLabelSwingWorker;
import mpp.SwingWorker.DoneAutoLabelSwingWorker;
import mpp.SwingWorker.DoneLoadImageSwingWorker;
import mpp.SwingWorker.LoadImageSwingWorker;
import mpp.SwingWorker.MultipleLoadImageSwingWorker;
import mpp.SwingWorker.TransformSwingWorker;
import mpp.imagenes.db.ImageDB;
import mpp.imagenes.db.LabelledImage;
import mpp.imagenes.labels.LabelDetector;
import mpp.imagenes.labels.LabelDetector.Label;
import mpp.imagenes.labels.MockLabelDetector;

/**
 * 
 * @author jesus
 *
 */
public class Controller {

	@Nonnull
	private final ImageDB db;
	@Nonnull
	private final ImageManager view;
	@Nonnull
	private final LabelDetector labelDetector = new MockLabelDetector();
	@CheckForNull
	private LabelledImage currentImage;

	// Pool de hilos
	private Executor pool;

	// Instante en el que se comienza a realizar la toma de tiempos
	private long startTime;

	// Lista de tareas completadas y por hacer
	private double completedTask = 0;
	private double todoTask = 0;

	// Calculo de hilos disponibles
//	 private static final int thread = Runtime.getRuntime().availableProcessors();
	private static final int thread = 12;

	private List<String> error;

	public Controller(@Nonnull ImageManager view) {
		this.view = view;
		this.db = new ImageDB();
		// Inicializamos la pool
		this.pool = Executors.newFixedThreadPool(thread);
		// this.pool = Executors.newCachedThreadPool();
		this.error = new LinkedList<String>();
		System.out.println("N�mero de hilos disponibles: " + thread);
	}

	public LabelledImage setCurrentImage(File file) {
		LabelledImage img = db.getImage(file);
		if (img == null)
			return null;
		this.currentImage = img;
		return img;
	}

	public LabelledImage getCurrentImage() {
		return currentImage;
	}

	public void loadScaledImage(@Nonnull File f, @Nonnegative int width, @Nonnegative int height) throws IOException {
		// Comenzamos el timer
		startTimer();
		this.todoTask += 1;
		Image image = loadScaled(f, width, height);
		if (image == null) {
			// reportar
			return;
		}
		db.addImage(f);
		view.addThumbnail(image, f);
		incrementWaitTask();
	}

	// Funci�n para cargar una imagen en segundo plano
	public void loadScaledImageSegundoPlano(@Nonnull File f, @Nonnegative int width, @Nonnegative int height)
			throws IOException {
		// Comenzamos el timer
		startTimer();
		// Comprobamos si la pool esta correcta
		checkPool();
		// Creamos el hilo y lo ejecutamos
		this.todoTask += 1;
		LoadImageSwingWorker sw = new LoadImageSwingWorker(f, width, height, this);
		pool.execute(sw);
	}

	public void loadScaledImages(@Nonnull List<? extends File> files, @Nonnegative int width, @Nonnegative int height)
			throws IOException {
		startTimer();
		this.todoTask += files.size();
		for (File file : files) {
			Image img = loadScaled(file, width, height);
			// reportar!
			if (img == null)
				continue;
			saveImg(file, img);
		}
	}

	// Funci�n para que un �nico hilo, sea capaz de cargar todo un directorio en
	// segundo plano
	public void loadScaledImagesUnico(List<File> files, int width, int height) {
		// Comenzamos el timer
		startTimer();
		// Comprobamos si la pool esta correcta
		checkPool();
		// Creamos el hilo y lo ejecutamos
		this.todoTask += files.size();
		MultipleLoadImageSwingWorker sw = new MultipleLoadImageSwingWorker((List<File>) files, width, height, this);
		pool.execute(sw);
	}

	// Funci�n para que un �nico hilo, sea capaz de cargar todo un directorio en
	// segundo plano, ha excepci�n del guardado en save
	public void loadScaledImagesUnicoDone(List<File> files, int width, int height) {
		// Comenzamos el timer
		startTimer();
		// Comprobamos si la pool esta correcta
		checkPool();
		// Creamos el hilo y lo ejecutamos
		this.todoTask += files.size();
		DoneLoadImageSwingWorker sw = new DoneLoadImageSwingWorker((List<File>) files, width, height, this);
		pool.execute(sw);
	}

	// Funci�n para cargar las imagenes mediante multiples hilos
	public void loadScaledImagesMultiple(List<File> files, int width, int height) {
		// Comenzamos el timer
		startTimer();
		// Comprobamos si la pool esta correcta
		checkPool();
		// Creamos los hilos y los encolamos
		this.todoTask += files.size();
		for (File file : files) {
			LoadImageSwingWorker sw = new LoadImageSwingWorker(file, width, height, this);
			pool.execute(sw);
		}
	}

	// http://www.java2s.com/Code/Java/2D-Graphics-GUI/ImageFilter.htm
	public void transformAll(@Nonnull File folder) {
		// Comenzamos el timer
		startTimer();
		this.todoTask += db.getImages().size();
		for (LabelledImage labelledImage : db.getImages()) {
			transformFile(folder, labelledImage);
		}
	}

	// Funci�n para realizar la transformaci�n en segundo plano
	public void transformAllSegundoPlano(@Nonnull File folder) {
		TransformSwingWorker sw;
		// Comenzamos el timer
		startTimer();
		// Comprobamos si la pool esta correcta
		checkPool();
		// Creamos los hilos y los encolamos
		this.todoTask += db.getImages().size();
		for (LabelledImage labelledImage : db.getImages()) {
			sw = new TransformSwingWorker(folder, labelledImage, this);
			pool.execute(sw);
		}
	}

	private void transformFile(File folder, LabelledImage labelledImage) {
		File f = labelledImage.getFile();
		try {
			BufferedImage image = ImageIO.read(f);
			float[] blurMatrix = { 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f,
					1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f };
			BufferedImageOp blurFilter = new ConvolveOp(new Kernel(3, 3, blurMatrix), ConvolveOp.EDGE_NO_OP, null);

			BufferedImage filtered = blurFilter.filter(image, null);

			File outputFileName = Paths.get(folder.getPath(), f.getName()).toFile();
			ImageIO.write(filtered, "jpg", outputFileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		incrementWaitTask();
	}

	@CheckForNull
	protected Image loadScaled(@Nonnull File f, @Nonnegative int w, @Nonnegative int h) throws IOException {
		System.out.println("Processing: " + f);
		BufferedImage img = ImageIO.read(f);
		if (img == null)
			return null;
		return img.getScaledInstance(w, h, Image.SCALE_FAST);
	}

	public void stopBackgroundTasks() {
		// Paramos la pool, y reiniciamos contadores
		if (!canceledPool()) {
			((ExecutorService) pool).shutdownNow();
			this.todoTask = 0;
			this.completedTask = 0;
			updateInfo();
		}
	}

	public void autoLabel() {
		// Comenzamos el timer
		startTimer();
		// Creamos los hilos y los encolamos
		this.todoTask += db.getImages().size();
		for (LabelledImage img : db.getImages()) {
			try {
				List<Label> labels = labelDetector.label(img.getFile());
				for (Label label : labels) {
					img.addLabel(label.getValue());
				}
				incrementWaitTask();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		updateInfo();
	}

	// Funci�n para realizar el autoetiquetado en segundo plano
	public void autoLabelSegundoPlano() {
		// Comenzamos el timer
		startTimer();
		// Comprobamos si la pool esta correcta
		checkPool();
		// Creamos los hilos y los encolamos
		int imagenesAModificar = db.getImages().size();
		this.todoTask += imagenesAModificar;
		AutoLabelSwingWorker sw = new AutoLabelSwingWorker(this, imagenesAModificar);
		pool.execute(sw);
	}

	// Funci�n para realizar el autoetiquetado en segundo plano mediante el done,
	// para evitar synchronize
	public void autoLabelSegundoPlanoDone() {
		// Comenzamos el timer
		startTimer();
		// Comprobamos si la pool esta correcta
		checkPool();
		// Creamos los hilos y los encolamos
		int imagenesAModificar = db.getImages().size();
		this.todoTask += imagenesAModificar;
		DoneAutoLabelSwingWorker sw = new DoneAutoLabelSwingWorker(this, imagenesAModificar);
		pool.execute(sw);
	}

	// Funciones de ayuda

	// Funci�n que comprueba si la pool esta activa, en caso de que no, la crea
	public void checkPool() {
		if (canceledPool())
			pool = Executors.newFixedThreadPool(thread);
	}

	public boolean canceledPool() {
		return (pool == null || ((ExecutorService) pool).isShutdown());
	}

	// Iniciamos el registro de tiempos
	public void startTimer() {
		this.startTime = System.currentTimeMillis();
	}

	// Termina la toma de tiempos
	public synchronized void endTimer() {
		long tiempo = System.currentTimeMillis() - startTime;
		long minutes = (tiempo / 1000) / 60;
		long seconds = (tiempo / 1000) % 60;
		long miliseconds = tiempo - (minutes * 60 * 1000 + seconds * 1000);

		System.out.println(
				"Duraci�n: " + minutes + " minutos y " + seconds + " segundos " + miliseconds + " milisegundos");
		this.startTime = 0;
	}

	// Funcin para guardar la imagen y aadir el thumbnail
	public void saveImg(File f, Image image) {
		saveDB(f);
		addThumbnail(f, image);
		incrementWaitTask();
	}

	// Funci�n para guardar en la base de datos
	public synchronized void saveDB(File f) {
		db.addImage(f);
	}

	// Funci�n para a�adir miniatura
	public void addThumbnail(File f, Image image) {
		view.addThumbnail(image, f);
	}

	// Actualizamos la informaci�n que mostramos
	public void updateInfo() {
		if (getTaskRestantes() > 0)
			view.setInfo("Procesando quedan " + getTaskRestantes() + " por terminar", getProgress());
		else {
			view.setInfo("Hemos terminado", getProgress());
			if (this.startTime != 0)
				endTimer();
			todoTask = 0;
			completedTask = 0;
			if (error.size() > 0) {
				view.openError(error);
				error = new LinkedList<String>();
			}
		}
	}

	// Obtenemos el n�mero de tareas restantes
	private int getTaskRestantes() {
		return (int) (todoTask - completedTask);
	}

	// Incrementamos el nmero de tareas hechas
	public synchronized int incrementWaitTask() {
		this.completedTask++;
		updateInfo();
		return (int) this.completedTask;
	}

	// Obtenemos el proceso
	public int getProgress() {
		int value;
		if (completedTask == 0 && todoTask == 0)
			value = 100;
		else if (completedTask == todoTask) {
			completedTask = 0;
			todoTask = 0;
			value = 100;
		} else {
			value = (int) (completedTask / todoTask * 100);
		}

		return value;
	}

	// Funci�n para obtener las imagenes desde un hilo
	public List<? extends LabelledImage> getDB() {
		return db.getImages();
	}

	// Funci�n para obtener las label de un archivo
	public List<Label> getLabel(File file) {
		try {
			return labelDetector.label(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// Funci�n para escribir las label de la imagen de manera sincronizada
	public synchronized void addLabel(LabelledImage img, String value) {
		img.addLabel(value);
	}

	// Funci�n para comprobar si una imagen es igual que la seleccionada
	public boolean isCurrentImg(LabelledImage img) {
		if (currentImage == null)
			return false;
		return currentImage.equals(img);
	}

	// Guardamos las label actuales, aunque no lo haya confirmado
	public void forceUpdate() {
		view.saveLabels();
	}

	// Forzamos a actualizar el panel
	public void updateTextPanel() {
		view.setLabel(currentImage.getHumanLabels());
	}

	// Mostrar errores
	public synchronized void addError(List<String> error) {
		// view.openError(error);
		this.error.addAll(error);
	}

}
