package mpp.imagenes;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import mpp.imagenes.db.LabelledImage;

public class ImageManager {

	private static final int HEIGHT = 50;
	private static final int WIDTH = 50;
	private JFrame frame;
	private JPanel figureListPanel;
	private JLabel pictureLabel;
	
	private Controller controller = new Controller(this);
	private JLabel lblTask;
	private JProgressBar progressBar;
	private JPanel infoPanel;
	private JTextField imageLabels;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImageManager window = new ImageManager();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ImageManager() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 950, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel p = new JPanel();
//		Toolbar original
		JToolBar toolBar = new JToolBar();
//		Toolbar para la carga de imagenes en segundo plano, tanto de una imagen como un directorio
		JToolBar toolBarCargarSegundoPlano = new JToolBar();
//		Toolbar para los botones relacionados con los filtros
		JToolBar toolBarFiltroSegundoPlano = new JToolBar();
//		Toolbar para los auto etiquetado en segundo plano
		JToolBar toolBarLabelSegundoPlano = new JToolBar();
		
		p.setLayout(new GridLayout(0,1));
		p.add(toolBar);
		p.add(toolBarCargarSegundoPlano);
		p.add(toolBarFiltroSegundoPlano);
		p.add(toolBarLabelSegundoPlano);
		frame.getContentPane().add(p, BorderLayout.NORTH);
		
		
		
		JButton btnLoad = new JButton("Cargar");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadImage();
			}
		});
		toolBar.add(btnLoad);
		
//		Nuevo boton para cargar en segundo plano una imagen
		JButton btnLoadSegundoPlano = new JButton("Cargar segundo plano");
		btnLoadSegundoPlano.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadImageSegundoPlano();
			}
		});
		toolBarCargarSegundoPlano.add(btnLoadSegundoPlano);
		
		JButton btnLoadBatch = new JButton("Cargar directorio");
		btnLoadBatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadImageBatch();
			}
		});
		toolBar.add(btnLoadBatch);
		
//		Boton que nos va a permitir cargar un directorio mediante el uso de varios hilos
		JButton btnLoadBatchMultipleHilos = new JButton("Cargar directorio multiples hilos");
		btnLoadBatchMultipleHilos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadImageBatchMultiple();
			}
		});
		toolBarCargarSegundoPlano.add(btnLoadBatchMultipleHilos);
		
//		Boton encargado de realizar la carga de un directorio en segundo plano, mediante el uso de un unico hilo
		JButton btnLoadBatchHiloUnico = new JButton("Cargar directorio hilo �nico");
		btnLoadBatchHiloUnico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadImageBatchUnico();
			}
		});

		toolBarCargarSegundoPlano.add(btnLoadBatchHiloUnico);

		JButton btnAutoEtiquetado = new JButton("Auto-etiquetado");
		btnAutoEtiquetado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doAutoLabelling();
			}
		});
		toolBar.add(btnAutoEtiquetado);
		
//		Boton para cargar un directorio mediante un hilo, pero a diferencia del anterior, este la carga se realizara en el hilo principal
		JButton btnLoadBatchHiloDone = new JButton("Cargar directorio done");
		btnLoadBatchHiloDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadImageBatchDone();
			}
		});
		toolBarCargarSegundoPlano.add(btnLoadBatchHiloDone);

//		Boton para realizar el etiquetado en segundo plano
		JButton btnAutoEtiquetadoSegundoPlano = new JButton("Auto-etiquetado segundo plano");
		btnAutoEtiquetadoSegundoPlano.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doAutoLabellingSegundoPlano();
			}
		});
		toolBarLabelSegundoPlano.add(btnAutoEtiquetadoSegundoPlano);
		
//		Boton para realizar el etiquetado en segundo plano pero mediante el uso del done, es decir, en el hilo de eventos
		JButton btnAutoEtiquetadoSegundoPlanoDone = new JButton("Auto-etiquetado segundo plano Done");
		btnAutoEtiquetadoSegundoPlanoDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doAutoLabellingSegundoPlanoDone();
			}
		});
		toolBarLabelSegundoPlano.add(btnAutoEtiquetadoSegundoPlanoDone);

		
		JButton btnAplicarFiltro = new JButton("Aplicar filtro");
		btnAplicarFiltro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doTransform();
			}
		});
		toolBar.add(btnAplicarFiltro);
		
//		Boton que nos permite aplicar un filtro en segundo plano
		JButton btnAplicarFiltroSegundoPlano = new JButton("Aplicar filtro segundo plano");
		btnAplicarFiltroSegundoPlano.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doTransformSegundoPlano();
			}
		});
		toolBarFiltroSegundoPlano.add(btnAplicarFiltroSegundoPlano);
		
		
		JPanel picturePane = new JPanel();
		JScrollPane pictureScrollPane = new JScrollPane(picturePane);
		picturePane.setLayout(new BorderLayout(0, 0));
		pictureLabel = new JLabel();
		picturePane.add(pictureLabel, BorderLayout.CENTER);
		imageLabels = new JTextField();
		imageLabels.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == '\n')
					saveLabels();
			}
		});
		
		picturePane.add(imageLabels, BorderLayout.NORTH);
		
		
		figureListPanel = new JPanel();
		
		// https://docs.oracle.com/javase/tutorial/uiswing/layout/grid.html
		figureListPanel.setLayout(new GridLayout(0, 1, 0, 0));	
		
		JScrollPane figureScroll = new JScrollPane(figureListPanel);
		
		//Create a split pane with the two scroll panes in it.
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				figureScroll, pictureScrollPane);
		splitPane.setOneTouchExpandable(true);
		splitPane.setDividerLocation(150);
		frame.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		
		//Provide minimum sizes for the two components in the split pane
		Dimension minimumSize = new Dimension(100, 50);
		figureListPanel.setMinimumSize(minimumSize);
		
		infoPanel = new JPanel();
		frame.getContentPane().add(infoPanel, BorderLayout.SOUTH);
		infoPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		lblTask = new JLabel("");
		infoPanel.add(lblTask);
		
		progressBar = new JProgressBar();
		infoPanel.add(progressBar);
		
		JButton btnStopTask = new JButton("Detener");
		btnStopTask.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				stopTasks();
			}
		});
		infoPanel.add(btnStopTask);
		picturePane.setMinimumSize(minimumSize);
	}
	
//	Funciones para llamar al controllador para realizar el etiquetado

	protected void doAutoLabellingSegundoPlano() {
		controller.autoLabelSegundoPlano();
	}
	
	protected void doAutoLabellingSegundoPlanoDone() {
		controller.autoLabelSegundoPlanoDone();
	}

//	Funcion para realizar la transformacion en segundo plano
	protected void doTransformSegundoPlano() {
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			File folder = fc.getSelectedFile();
			controller.transformAllSegundoPlano(folder);
		}
	}

	//	Nueva funci�n para llamar a la nueva funci�n del controllador
	protected void loadImageSegundoPlano() {
		final JFileChooser fc = new JFileChooser();
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			File f = fc.getSelectedFile();
			try {
				controller.loadScaledImageSegundoPlano(f, WIDTH, HEIGHT);
			} catch (IOException e) {
			}
		}
	}
	
//	Nueva funci�n para un hilo �nico
	protected void loadImageBatchUnico() {
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			File folder = fc.getSelectedFile();
			try {
				Stream<Path> files = Files.walk(folder.toPath());
				List<File> fileList = files
						.map(Path::toFile)
						.filter(f_ -> f_.isFile())
						.collect(Collectors.toList());
				files.close();
				controller.loadScaledImagesUnico(fileList, WIDTH, HEIGHT);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
//	Nueva funci�n para varios hilos
	protected void loadImageBatchMultiple() {
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			File folder = fc.getSelectedFile();
			try {
				Stream<Path> files = Files.walk(folder.toPath());
				List<File> fileList = files
						.map(Path::toFile)
						.filter(f_ -> f_.isFile())
						.collect(Collectors.toList());
				files.close();
				controller.loadScaledImagesMultiple(fileList, WIDTH, HEIGHT);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	

//	Nueva funci�n para con un hilo, y guardar en el done
	protected void loadImageBatchDone() {
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			File folder = fc.getSelectedFile();
			try {
				Stream<Path> files = Files.walk(folder.toPath());
				List<File> fileList = files
						.map(Path::toFile)
						.filter(f_ -> f_.isFile())
						.collect(Collectors.toList());
				files.close();
				controller.loadScaledImagesUnicoDone(fileList, WIDTH, HEIGHT);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}

	protected void saveLabels() {
		LabelledImage currentImage = controller.getCurrentImage();
		if (currentImage != null) {
			currentImage.setHumanLabels(imageLabels.getText());
		}
	}

	protected void doAutoLabelling() {
		controller.autoLabel();
	}

	/**
	 * Aplica una transformación a todas las imagenes cargadas
	 */
	protected void doTransform() {
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			File folder = fc.getSelectedFile();
			controller.transformAll(folder);
		}
	}

	protected void loadImageBatch() {
		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			File folder = fc.getSelectedFile();
			try {
				Stream<Path> files = Files.walk(folder.toPath());
				List<File> fileList = files
						.map(Path::toFile)
						.filter(f_ -> f_.isFile())
						.collect(Collectors.toList());
				files.close();
				controller.loadScaledImages(fileList, WIDTH, HEIGHT);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}

	protected void loadImage() {
		final JFileChooser fc = new JFileChooser();
		if (fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
			File f = fc.getSelectedFile();
			try {
				controller.loadScaledImage(f, WIDTH, HEIGHT);
			} catch (IOException e) {
			}
		}
	}
	
	protected void stopTasks() {
		controller.stopBackgroundTasks();
	}
	
	private synchronized void addToPanel(ImageIcon icon, File file) {
	    JLabel picLabel = new JLabel(icon);
	    figureListPanel.add(picLabel);
	    picLabel.addMouseListener(new MouseAdapter() {
	    	@Override
	    	public void mouseClicked(MouseEvent e) {
	    		File loadedFile = file;
	    		
				try {
		    		BufferedImage img = ImageIO.read(loadedFile);
		    		LabelledImage image = controller.setCurrentImage(file);
					if (image == null)
						return;
					setLabel(image.getHumanLabels());
		    		pictureLabel.setIcon(new ImageIcon(img));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	    		
	    	}
	    });
	}
	
	public void addThumbnail(Image image, File f) {
		addThumbnail(new ImageIcon(image), f);
	}
	
	public void addThumbnail(ImageIcon image, File f) {
		addToPanel(image, f);
		figureListPanel.revalidate();
	}

	public void setInfo(String taskDescription, int progress) {
		lblTask.setText(taskDescription);
		if (progress >= 0 && progress <= 100)
			progressBar.setValue(progress);
	}
	
//	Forzado para escribir las etiquetas en el text panel correspondiente
	public void setLabel(String value) {
		imageLabels.setText(value);
	}
	

//	Mostrar option pane con los errores
	public void openError(List<String> error) {
		String msg = "La siguiente lista de archivos no se ha podido cargar\n";
		for (String string : error) {
			msg += "- "+string+"\n";
		}
		JOptionPane.showMessageDialog(null, msg,
			      "Error al procesar los siguientes ficheros", JOptionPane.ERROR_MESSAGE);
	}

}
